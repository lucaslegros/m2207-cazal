package tp6_cazal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import tp5_cazal.CompteurDeClic;

public class PanneauServeur extends JFrame implements ActionListener
{
	//Attributs

	private JTextArea texte;
	private Container panneau;
	private JButton bouton;
	private JScrollPane scorllPane;
	private Socket SocketClient;
	private ServerSocket serveur;
	private BufferedReader save;
	private String ligne;
	
	//Constructeur

	public PanneauServeur()
	{
		super();
		this.setTitle("Serveur - Panneau d'affichage");
		this.setSize(400,300);
		this.bouton = new JButton("Exit");
		this.texte = new JTextArea("Le panneau d'affichage est actif\n", 24, 12);
		this.scorllPane = new JScrollPane(this.texte);
		this.panneau = getContentPane();
		this.panneau.add(this.bouton, BorderLayout.SOUTH);
		this.panneau.add(this.scorllPane, BorderLayout.CENTER);
		this.bouton.addActionListener((ActionListener) this);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try
		{
			serveur = new ServerSocket(8888);
			this.texte.append("Serveur d�marr�\n");
		}
		catch (Exception e)
		{
			this.texte.append("Erreur de cr�ation ServerSocket\n");
		}
		this.setVisible(true);
		ecouter();
	}
	
	//M�thode
	
	public void ecouter()
	{
		try
		{
			this.texte.append("Serveur en attente de connexion...\n");
			SocketClient = serveur.accept();
			this.texte.append("Client Connect�\n");
			save = new BufferedReader(new InputStreamReader(SocketClient.getInputStream()));
			while ((ligne = save.readLine()) != null)
			{
				this.texte.append("Message re�u :"+ligne+"\n");
			}
			SocketClient.close();
			serveur.close();
		}
		catch (Exception e)
		{
			
		}
		
	}
	
	//M�thode main

	public static void main(String[] args)
	{
		PanneauServeur app = new PanneauServeur();
	}
	public void actionPerformed(ActionEvent e)
	{
		System.exit(-1);
	}
}