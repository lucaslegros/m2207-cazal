package tp6_cazal;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class MonClient {

	public static void main(String[] args) 
	{
		ServerSocket monServerSocket;
		Socket monSocket;
		PrintWriter monPrintWriter;
		
		try
		{
			monServerSocket = new ServerSocket(1234);
			monSocket = new Socket("localhost", 8888);
			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			System.out.println("Envoie du message : Hello World");
			monPrintWriter.println("Hello World");
			monPrintWriter.flush();
			System.out.println("Client: "+monSocket);
			monSocket.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
