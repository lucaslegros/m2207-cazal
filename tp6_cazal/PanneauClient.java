package tp6_cazal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class PanneauClient extends JFrame implements ActionListener {
	
	//Attributs

	private JTextField texte;
	private Container panneau;
	private JButton bouton;
	private Socket monSocket;
	private PrintWriter monPrintWriter;
	
	//Constructeur

	public PanneauClient()
	{
		super();
		this.setTitle("Client - Panneau d'affichage");
		this.setSize(250,120);
		
		this.texte = new JTextField();
		this.bouton = new JButton("Envoyer");
		this.panneau = getContentPane();
		
		this.panneau.setLayout(new FlowLayout());
		
		this.panneau.setLayout(new GridLayout(3,1,5,5)); 
		this.panneau.add(texte);
		this.panneau.add(bouton);
		this.bouton.addActionListener(this);
		try
		{
			
			monSocket = new Socket("localhost", 8888);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	//M�thode
	
	public void emettre()
	{
		System.out.println("Envoi d�un message");
		try
		{
			monPrintWriter = new PrintWriter(monSocket.getOutputStream());
			monPrintWriter.println(this.texte.getText());
			monPrintWriter.flush();
			monSocket.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	//M�thode main

	public static void main(String[] args)
	{
		PanneauClient app = new PanneauClient();
	}

	public void actionPerformed(ActionEvent arg0) 
	{
		emettre();
	}
}
