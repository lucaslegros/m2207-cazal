package tp2_cazal;

public class Cercle extends Forme{

	private double rayon;
	
	//constructeurs
	public Cercle(){
		super();
		rayon = 1.0;
	}
	
	public Cercle(double r){
		rayon = r;
	}
	
	public Cercle(double r, String couleur, boolean coloriage){
		super(couleur, coloriage);
		rayon = r;
	}
	
	
	//accesseurs
	public double getRayon(){
		return this.rayon;
	}
	
	public void setRayon(double r){
		this.rayon=r;
	}
	
	
	//M�thodes
	public String seDecrire(){
		return "un cercle de rayon " + rayon + " " +super.seDecrire();
	}
	
	public double calculerAire(){
		return Math.PI*rayon*rayon;
	}
	
	public double calculerPerimetre(){
		return 2*Math.PI*rayon;
	}
}
