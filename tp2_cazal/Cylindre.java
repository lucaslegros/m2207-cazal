package tp2_cazal;

public class Cylindre extends Cercle{
	
	double hauteur;
	
	//Constructeurs
	public Cylindre(){
		hauteur = 1.0;
	}
	
	public Cylindre(double r, double h, String couleur, boolean coloriage){
		super(r,couleur, coloriage);
		h = hauteur;
	}
	
	//M�thodes
	public String seDecrire(){
		return "un cylindre de rayon " + this.getRayon() + "une forme de couleur " + this.getCouleur() + " et de coloriage " + this.isColoriage() + " et de hauteur " + hauteur;
	}
	
	public double calculerVolume(){
		return Math.PI*this.getRayon()*this.getRayon()*hauteur;
	}

}
