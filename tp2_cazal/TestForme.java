package tp2_cazal;

public class TestForme {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//déclarations
		Forme f1;
		Forme f2;
		Cercle c1;
		Cercle c2;
		Cercle c3;
		Cylindre cy1;
		Cylindre cy2;
		
		//instanciation
		f1 = new Forme("rouge", false);
		f2 = new Forme("vert", false);
		c1 = new Cercle();
		c2 = new Cercle();
		c3 = new Cercle(3.2,"jaune", false);
		cy1 = new Cylindre();
		cy2 = new Cylindre(4.2,1.3,"bleu",true);
		
		//Affichage
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage() );
		System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage() );
		System.out.println(f1.seDecrire());
		System.out.println(f2.seDecrire());
		System.out.println(c1.seDecrire());
		c2.setRayon(2.5);
		System.out.println(c2.seDecrire());
		System.out.println(c3.seDecrire());
		
		System.out.println("Aire c2 = " + c2.calculerAire() + " et c3 = "+c3.calculerAire());
		System.out.println("Périmètre c2 = " + c2.calculerPerimetre()+ " et c3 = "+c3.calculerPerimetre());
		System.out.println(cy1.seDecrire());
		System.out.println(cy2.seDecrire());
		System.out.println("Volume cy1 = "+cy1.calculerVolume()+" et cy2 = "+cy2.calculerVolume());
	}

}
