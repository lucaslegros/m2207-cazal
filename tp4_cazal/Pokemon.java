package tp4_cazal;

public class Pokemon {

	//attributs
	private int energie, maxEnergie, cycle, puissance;
	private String nom;
	
	
	//accesseurs
	public String getNom(){
		return nom;
	}
	
	public int getEnergie(){
		return energie;
	}
	
	public int getPuissance(){
		return puissance;
	}
	
	//constructeurs
	public Pokemon(String nom){
		this.nom = nom;
		maxEnergie = 50 + (int) (Math.random()*((90 - 50) + 1));
		energie = 30 + (int)(Math.random()*((maxEnergie - 30) + 1));
		puissance = 3 + (int)(Math.random()*((10 - 3) + 1));
	}
	
	//m�thodes
	public void sePresenter(){
		System.out.println("Je suis " + nom + ", j'ai " + energie + " points d'�nergie (" + maxEnergie +" max) et une puissance de "+ puissance);
	}
	
	public void manger(){
		energie = energie + (10 +(int)(Math.random()*((30 - 10) + 1)));

		if(energie>maxEnergie){
			energie = maxEnergie;
		}
		else{
			sePresenter();
			System.out.println( nom + " n'a plus faim, " + nom + " jette la nourriture ");
		}
	}
	
	public void vivre(){
		energie = energie - (20 +(int)(Math.random()*((40 - 20) + 1)));
		sePresenter();
	}
	
	public boolean isAlive(){
		if(energie>0){
			return true;
		}else{
			return false;
		}
	}
	
	public void cycle(){
		cycle = 0;
		while(isAlive() == true){
			manger();
			vivre();
			cycle = cycle + 1;
		}
		System.out.println(nom + " a v�cu " + cycle + " cycles !");
	}
	
	public void perdreEnergie(int perte){
		if(energie<2.5*maxEnergie){
			energie = (int) (energie - 1.5*perte);
		}
		else{
			energie = energie - perte;
		}
	}
}
