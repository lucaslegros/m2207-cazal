package tp5_cazal;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

public class MonAppliGraphique extends JFrame
{
	//Attributs

	private Container panneau;
	private JButton b, c, d, e, f;
	private JLabel monLabel;
	private JTextField monTextField;

	//Constructeur

	public MonAppliGraphique()
	{
		super();
		this.setTitle("MA premi�re application"); 
		this.setSize(400,200); 
		this.setLocation(20,20); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		b = new JButton("Bouton 0");
		c = new JButton("Bouton 1");
		d = new JButton("Bouton 2");
		e = new JButton("Bouton 3");
		f = new JButton("Bouton 4");
		this.monLabel = new JLabel("Je suis un JLabel");
		this.monTextField = new JTextField("Je suis un JTextField"); 
		this.panneau = getContentPane(); 
		this.panneau.setLayout(new GridLayout(3,5)); 
		this.panneau.add(b, BorderLayout.CENTER); 
		this.panneau.add(c, BorderLayout.NORTH);  
		this.panneau.add(d, BorderLayout.EAST); 
		this.panneau.add(e, BorderLayout.SOUTH); 
		this.panneau.add(f, BorderLayout.WEST);
		//this.panneau.add(monLabel);
		//this.panneau.add(monTextField);
		//this.panneau.setLayout(new FlowLayout()); 
		this.setVisible(true);

		System.out.println("La fen�tre est cr��e !");
	}
	
	//M�thode main

	public static void main(String[] args)
	{
		MonAppliGraphique app = new MonAppliGraphique(); 
	}
}