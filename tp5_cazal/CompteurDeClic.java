package tp5_cazal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CompteurDeClic extends JFrame implements ActionListener
{
	//Attributs

	private int compteur = 0;
	private Container panneau;
	private JButton bouton;
	private JLabel info;
	
	//Constructeur

	public CompteurDeClic()
	{
		super();
		this.setTitle("MA premi�re application"); 
		this.setSize(200,100); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		bouton = new JButton("Click !");
		this.info = new JLabel("Vous avez cliqu� "+this.compteur+" fois"); 
		this.panneau = getContentPane(); 
		this.panneau.add(bouton);
		this.panneau.add(info);
		this.panneau.setLayout(new FlowLayout()); 
		this.bouton.addActionListener(this);
		this.setVisible(true);
	}
	
	//M�thode main

	public static void main(String[] args)
	{
		CompteurDeClic app = new CompteurDeClic();
	}

	public void actionPerformed(ActionEvent e) 
	{
		System.out.println("Une action a �t� d�tect�e");
		this.compteur ++;
		this.info.setText("Vous avez cliqu� "+this.compteur+" fois"); 
	}
}

