package tp5_cazal;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PlusOuMoinsCher extends JFrame implements ActionListener
{
	//Attributs

	private int nbr, tentative;
	private Container panneau;
	private JButton bouton;
	private JLabel ml1, ml2;
	private JTextField monTextField;
	
	//Constructeur

	public PlusOuMoinsCher()
	{
		super();
		this.setTitle("Plus cher ou moins cher"); 
		this.setSize(350,150); 
		this.setLocation(300,150); 
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		this.ml1 = new JLabel("Votre proposition :");
		this.ml2 = new JLabel("La r�ponse");
		this.bouton = new JButton("V�rifie !");
		this.monTextField = new JTextField("32");
		this.panneau = getContentPane(); 
		this.panneau.setLayout(new GridLayout(2,2)); 
		this.init();
		this.bouton.addActionListener(this);
		this.setVisible(true);
	}
	
	//M�thode
	
	public void init()
	{
		
		this.tentative = 0;
		
		this.panneau.add(ml1, BorderLayout.NORTH);
		this.panneau.add(monTextField, BorderLayout.EAST);
		this.panneau.add(bouton, BorderLayout.WEST);
		this.panneau.add(ml2, BorderLayout.SOUTH);
		
		nbr = 0+(int)(Math.random()*((100-0)+1));
		
		System.out.println("Nombre al�atoire a trouver : "+this.nbr);
		this.ml2.setText("La r�ponse");
		this.monTextField.setText("32");
	}

	//M�thode main

	public static void main(String[] args)
	{
		PlusOuMoinsCher app = new PlusOuMoinsCher();
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		this.tentative ++;
		System.out.println("Tentative = "+this.tentative);
		System.out.println("Nombre entr� : "+this.monTextField.getText());
		
		if (this.nbr > Integer.parseInt(this.monTextField.getText()))
		{
			this.ml2.setText("Plus cher !"); 
		}
		else if (this.nbr < Integer.parseInt(this.monTextField.getText()))
		{
			this.ml2.setText("Moins cher !"); 
		}
		else
		{
			this.ml2.setText("Vous avez devin� !"); 
			this.init();
		}
		
		if (this.tentative == 8)
		{
			this.ml2.setText("Vous avez fait trop de tentative");
			this.init();
		}
	}

}
