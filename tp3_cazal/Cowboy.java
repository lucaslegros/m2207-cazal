package tp3_cazal;

public class Cowboy extends Humain{

	protected int popularite;
	protected String caracteristique;
	
	//constructeur
	public Cowboy(String nom){
		super(nom);
		boissonFav = "Whiskey";
		popularite = 0;
		caracteristique = "vaillant";
	}
	
	//m�thode
	public void tire(Brigand brigand){
		System.out.println("Le " + caracteristique +" "+ this.nom + " tire sur " + brigand.nom +". PAN !"); 
		parler("Prends �a, voyou !");
	}
	
	public void libere(Dame dame){
		popularite = popularite + 10;
	}
}
