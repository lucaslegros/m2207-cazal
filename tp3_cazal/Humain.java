package tp3_cazal;

public class Humain {

	protected String nom, boissonFav;
	
	//Constructeurs
	public Humain(String nom){
		this.nom = nom;
		boissonFav = "lait";
	}
	
	//M�thodes
	public String quelEstTonNom(){
		return this.nom;
	}
	
	public String quelleEstTaBoisson(){
		return this.boissonFav;
	}
	
	public void parler(String texte){
		System.out.println("("+ nom + ")" + " - " + texte);
	}
	
	public void sePresenter(){
		parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson pr�f�r�e est le " + quelleEstTaBoisson());
	}
	
	public void boire(){
		parler("Ah ! Un bon verre de " + quelleEstTaBoisson() + " ! GLOUPS !");
	}
}
