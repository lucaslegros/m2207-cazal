package tp3_cazal;

public class Brigand extends Humain{

	//attributs
	private String look;
	private int nbDameKidnap, recompense;
	private boolean libre;
	
	//constructeurs
	public Brigand(String nom){
		super(nom);
		look = "m�chant";
		libre = true;
		recompense = 100;
		nbDameKidnap = 0;
		boissonFav = "Cognac";
	}
	
	//m�thodes
	public int getRecompense(){
		return recompense;
	}
	
	public String quelEstTonNom(){
		return nom + " le " + look;
	}
	
	public void sePresenter(){
		super.sePresenter();
		parler("J'ai l'air " + look + " et j'ai enlev� " + nbDameKidnap + " dames.");
		parler("Ma t�te est mise � prix " + recompense + "$");
	}
	
	public void enleve(Dame dame){
		recompense = recompense + 100;
		nbDameKidnap = nbDameKidnap + 1;
		dame.libre = false;
		parler("Ah ah! " + dame.nom + ", tu es ma prisonni�re !");
	}

	public void emprisonner(Sherif sherif) {
		parler("Damned, je suis fait ! " + sherif.nom + ", tu m'as eu !");
		
	}
}
