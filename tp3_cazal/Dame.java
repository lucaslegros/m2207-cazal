package tp3_cazal;

public class Dame extends Humain{

	//attributs
	boolean libre;
	private String situation;
	
	//Constructeurs
	public Dame(String nom){
		super(nom);
		boissonFav = "Martini";
		libre = true;
	}
	
	//m�thodes
	public void priseEnOtage(){
		libre = false;
		parler("Au secours !");
	}
	
	public void estLiberee(){
		libre = true;
		parler("Merci cowboy");
	}
	
	public String quelEstTonNom(){
		return "Miss " + nom;
	}
	
	public void sePresenter(){
		super.sePresenter();
		
		if(libre = true){
			situation = "libre";
		}
		else{
			situation = "kidnapp�e";
		}
		
		parler("Actuellement, je suis " + situation);
	}
}
