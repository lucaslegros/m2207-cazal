package tp3_cazal;

import tp3.Sherif;

public class Histoire {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Déclarations
		Humain humain;
		Dame dame;
		Brigand brigand;
		Cowboy cowboy;
		Sherif sherif;
		
		//instanciation
		humain = new Humain("Bob");
		dame = new Dame("Rachel");
		brigand = new Brigand("Gunther");
		cowboy = new Cowboy("Simon");
		sherif = new Sherif("Marshall");
		
		//Affichage
		humain.sePresenter();  //humain bob
		humain.boire();
		
		dame.sePresenter();  //dame Rachel
		brigand.sePresenter(); //brigand Gunther
		brigand.enleve(dame);
		dame.priseEnOtage();
		brigand.sePresenter();
		dame.sePresenter();
		cowboy.sePresenter(); //cowboy Simon
		cowboy.tire(brigand);
		dame.estLiberee();
		dame.sePresenter();
		sherif.sePresenter();
		sherif.coffrer(b);
		
		
		
		
		
		
		
	}

}
