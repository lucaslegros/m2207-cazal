package tp3_cazal;

public class Sherif extends Cowboy{

	//Attributs
	private int brigArrest;
	
	//Constructeurs
	public Sherif(String nom){
		super(nom);
		brigArrest = 0;
	}
	
	//M�thodes
	public String quelEstTonNom(){
		return "Sh�rif " + nom;
	}
	
	public void sePresenter(){
		super.sePresenter();
		parler("Je suis " + caracteristique + " et ma popularit� est " + popularite);
		parler("J'ai d�j� arr�t� " + brigArrest + " brigand(s)");
		
	}
	
	public void coffrer(Brigand b){
		b.emprisonner(this);
		brigArrest = brigArrest+1;
		parler("Au nom de la loi, je vous arr�te, " + b.nom + " !");
	}
}
